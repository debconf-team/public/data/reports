#!/usr/bin/python


# Overkill..... I know - MH

import pylab
from matplotlib.font_manager import FontProperties

debcolors = ['#6363BA', '#BBDDFF', '#8F3F56', '#FFD400', '#800080']

pylab.rc('font', size=10)
pylab.rc('xtick', labelsize=10)

# Event by track graph
eventnums = [8, 10, 110]
eventnames = ['DebCamp (8)', 'DebianDay (10)', 'DebConf (110)']

pylab.figure(figsize=(8,8))
pylab.axes([0.1, 0.1, 0.8, 0.8])
pylab.pie(eventnums, colors=debcolors)
pylab.legend(eventnames, loc=0, prop=FontProperties(size='smaller'))
pylab.title('DebConf 7 Events by Track')
pylab.savefig('eventtrack.png', dpi=100, format='png')

pylab.close()


# Event by track graph
dcevents = [45, 49, 16]
dceventtypes = ['Talks (45)', 'BOFs and Workshops (49)', 'Other (16)']

pylab.figure(figsize=(8,8))
pylab.axes([0.1, 0.1, 0.8, 0.8])
pylab.pie(dcevents, colors=debcolors)
pylab.legend(dceventtypes, loc=0, prop=FontProperties(size='smaller'))
pylab.title('DebConf Track Events by Type')
pylab.savefig('dceventtypes.png', dpi=100, format='png')

pylab.close()



# Food prefs (which we don't use)
food = [184, 37, 9, 3]
foodtypes = ['Regular', 'Vegetarian', 'Vegan', 'Other']

pylab.figure(figsize=(8,8))
pylab.axes([0.1, 0.1, 0.8, 0.8])
pylab.pie(food, colors=debcolors)
pylab.legend(foodtypes, loc=0, prop=FontProperties(size='smaller'))
pylab.title('Food Preferences')
pylab.savefig('foodprefs.png', dpi=100, format='png')

pylab.close()


# Participants by country
countnumbers = [52, 27, 26, 10, 9, 8, 7, 6, 6, 6, 6, 5, 35]

countries = ['United Kingdom', 'Germany', 'United States of America', 'Venezuela', 'Spain', 'France', 'Argentina', 'Brazil', 'Finland', 'Mexico', 'Sweden', 'Norway', 'Other']

pylab.figure(figsize=(8,8))
pylab.axes([0.1, 0.1, 0.8, 0.8])
pylab.pie(countnumbers, autopct='%.1f', colors=debcolors, labels=countries)
#pylab.legend(countries, loc=0)
pylab.title('Attendees by Country')
pylab.savefig('countries.png', dpi=100, format='png')

pylab.close()


# Participants by continent
cont = [140, 30, 27, 4]
contnames = ['Eurasia', 'South\nAmerica', 'North\nAmerica', 'Australia']

pylab.figure(figsize=(8,8))
pylab.axes([0.1, 0.1, 0.8, 0.8])
pylab.pie(cont, autopct='%.1f', colors=debcolors, labels=contnames)
#pylab.legend(countries, loc=0)
pylab.title('Attendees by Continent')
pylab.savefig('continents.png', dpi=100, format='png')

pylab.close()

types = [ 185, 78, 122, 31, 40]
typesnames = ['Debian Developer', 'Maintainer', 'Interested Person', 'Accompanying', 'Other']

pylab.figure(figsize=(8,8))
pylab.axes([0.1, 0.1, 0.8, 0.8])
pylab.pie(cont, autopct='%.1f', colors=debcolors, labels=typesnames)
#pylab.legend(countries, loc=0)
pylab.title('Attendees by Type')
pylab.savefig('type.png', dpi=100, format='png')

