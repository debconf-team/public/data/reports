%% See README.writers
%% Copyright 2014 Michael Banck
%% License: CC-BY-SA-3.0
%%
%% vim: set tw=80 background=light visualbell:
\section{Talks}

While face-to-face meetings and individual hacking time are more pronounced at
DebConf than at some other tech conferences, the main talks are nevertheless a
very important part of the conference.  This is further underlined by the fact
that remote participation has been traditionally an important part of the
conference and attending talks via video live stream is the primary way to do
so.  All official talks are accessible as live streams and their
recordings can be downloaded for viewing during or shortly after the conference.

The talks at DebConf are of a wide variety of topics (some of them
bundled into tracks) and DebConf14 was no exception.  Organization,
selection, and scheduling of the talks and similar events are done by
the talks team.

\begin{figure}[t]
  \centering
    \includegraphics[width=.35\textwidth]{images/photos/qanda_linus_torvalds}
    \hspace{0.1\textwidth}
    \includegraphics[width=.35\textwidth]{images/photos/keynote_anonymous}
    \\[0.5em]
    \parbox{.3\textwidth}{\imcap{Linus Torvalds in the Q\&A session}}
    \addcontentsline{pixaigarius}{section}{Linus Torvalds in the Q\&A session}
    \hspace{0.15\textwidth}
    \parbox{.3\textwidth}{\imcap{Prof.\ Coleman giving the keynote on Anonymous}}
    \addcontentsline{pixaigarius}{section}{Prof.\ Coleman giving the keynote on Anonymous}
\end{figure}


\subsection{Highlights}

It is hard to single out individual sessions from the fantastic
DebConf program. Some highlights stand out nevertheless:

\paragraph{Keynote on Anonymous by Prof.\ Gabriella Coleman} This year's
keynote had the rare feature of being both by a former DebConf main
organizer and the leading worldwide academic expert on an important
contemporary political and sociological issue: the history of Anonymous,
the decentralized and highly fluid hacker and hacktivist collective.  As
Wolfe Chair in Scientific and Technological Literacy at McGill
University, Toronto, Prof.\ Gabriella Coleman has followed Anonymous and
its various splinter groups and offshoots since nearly the beginning and
gave a fascinating speech on how Anonymous was born, how it evolved and
forked into various groups with varying political and technological
agendas.

\paragraph{Q\&A with Linus Torvalds} Linus Torvalds gives no talks, and
usually only attends LCA (Linux Conference Australia) and the Kernel
Summit. As a Portland resident, however, he could be convinced to
participate to DebConf by holding an evening Q\&A session.  There were
so many questions on various topics that the originally planned 45
minute session overran by more than 30 minutes.

\paragraph{Plenary talk by Stefano Zacchiroli} As a former Debian
Project Leader, Stefano Zacchiroli talked about “Debian in the Dark Ages
of Free Software”.  He explained how, even though Free and Open Source
Software is vastly successful in general and the Debian distribution is
doing well in particular, the rise of web services, where proprietary
code (usually JavaScript) is executed on the user's machine is a huge
problem.  His speech set the tone for related discussions during the
conference week.

\paragraph{Release Team status update plenary} On the final day of the
conference, Julien Cristau gave a talk on behalf of the release team,
updating the attendees on the status of the next Debian release and what
changes the release team has implemented for this release and plans for
the future.


\subsection{Schedule Structure}
\figgraph{b}{1.0}{images/photos/audience_clapping}{Satisfied audience at
  the end of a talk}{aigarius}
In some important ways the DebConf schedule was different from earlier editions of the
conference:

\paragraph{Concurrent Sessions} The number of concurrent talks was increased from
two to three.  However, usually at least one informal session (BoF, see below)
was scheduled in one of the three talk rooms.  Further, a last-minute BoF track
was established in one of the hacklabs by the attendees for sessions not
requiring a projector.

\paragraph{Plenaries} The sessions on the first and last day of DebConf were
keynote-type plenaries, i.e. had nothing concurrent scheduled against them.  For
the plenaries, a big auditorium hosting up to 400 people was used.

\paragraph{Hacking time and ad-hoc sessions} For the first time, some morning or
afternoon slots were reserved for hacking time and had no official main
sessions scheduled.  This allowed for either concentrated, uninterrupted work
sessions or a break from the intense conference schedule. Nevertheless, ad-hoc
sessions could be self-scheduled by attendees on the previous day at the
earliest during those times.  This allowed to discuss things which might have
only come up or for which interest was registered during DebConf itself.


\subsection{Types of Talks}

One of DebConf's distinctive features is its wide variety of session types.
At DebConf14 these ranged from keynotes for all attendees to informal
sessions:

\paragraph{Plenaries} The opening and closing ceremonies, the keynotes on the first
day, as well as the plenary Q\&A session with Linus Torvalds on Thursday, the
lightning talks, and release team talk on the last day were plenaries in the
ballroom auditorium.  No other sessions were scheduled at the same time to allow
all attendees of the conference to attend.

\paragraph{Main talks} The main talks consisted of the accepted talk
proposals and took place concurrently in three different rooms during the main session
slots.  The DebConf video team recorded all main talks and made them
available as live streams on the Internet.
Typically, they had only one speaker, although some sessions had several.

\paragraph{Lightning Talks and Live Demos} Lightning talks are quick 5~minute
presentations about a single topic.  As it is a convenient way for the audience
to learn about various things in a short time, the lightning talks were held in
the plenary auditorium this year.  The live demos are similar in that several
live demos fit in one session slot—they had a time limit of 20~minutes (but
not everybody used up the whole time).  In those demos, the participants ran a
live demonstration of some particular project or aspect they wanted to show,
which can potentially be very instructive to the audience.

\figgraph{t}{1.0}{images/photos/bof_discussion}{Technical discussion
  during a BoF}{aigarius}
\paragraph{BoFs} “Birds of a Feather” meetings are informal gatherings of
people sharing a common interest with an emphasis on attendee participation (although nobody
is forced to participate if they just want to watch and listen). Usually the
session organizer introduced the session with a small presentation, after
(or during) which a lengthy discussion ensues.

\paragraph{Ad-hoc sessions} Ad-hoc sessions were not those not scheduled prior to the
beginning of DebConf.  Typically, they were BoFs announced by people who realized during
DebConf that they share a common goal and/or should have a more focused
discussion in a larger group than just during the face-to-face “hallway
track”.  Some prepared talks which were too late for the Call for Papers
deadline were also scheduled during the ad-hoc sessions.


\subsection{Tracks}

Similar to previous DebConfs, thematically related talks were assigned
to specific tracks.  The tracks were identified by clear trends in
the submitted talks and highlight the participants' interest in a particular topics.

The consecutive scheduling of sessions belonging to the same track allowed for
interested people to easily follow the whole track and facilitated discussions
between the various presenters and participants.

Some of the tracks at DebConf14 were ``Cloud'', ``Validation and Continuous
Integration'', ``Security'', and ``Packaging and Tools''.


\subsection{Call for Papers and Selection Process}

The Call for Papers was published on June 8, and the deadline was one month
later.  After two weeks and due to a slow start in talk submissions, a first set
of talks were accepted and announced, resulting in an uptake of submissions.
Further reminders were sent throughout the Call for Papers period.  The last
reminder was sent three days before the deadline, which resulted in a surge
(around 30\%) of last-minute submissions.

Overall, 115 submissions were received by the deadline, compared to around 85
available talk slots.  This required the downselection of submissions
and consequently some proposals had to be rejected.  In a first round, each member of the talks selection team
marked those talks they thought should not be accepted or needed some discussion
before being accepted.  All talks not marked by at least one member
were accepted immediately.  Afterwards, the proposals considered
problematic by some members were discussed and decided upon in an online team
meeting. 


\subsection{Talks Teams}

The talks team consists of three distinct, but overlapping sub-teams. Their
tasks were selecting and scheduling talks and sessions.

\paragraph{Talks selection} The talks selection team's task is to send out the call
for papers (CfP) and to review the submissions after the deadline. They
followed a selection policy, which is described below.
\paragraph{Talks scheduling} After the accepted talks were chosen, they had to be
put on the schedule.  Scheduling involved two challenges: (i)
estimating which talks will be attended by how many people, as three different
room sizes were available and (ii) estimating which talks might be interesting
to the same set of people, so less people have to choose between two talks they
would like to see.
\paragraph{Ad-Hoc sessions scheduling} Finally, during DebConf, ad-hoc sessions were
possible during the allocated hacking time slots and for gaps in the schedule.
Those sessions were self-scheduled by the attendees on a wiki, but they were
organised and put on the official schedule by the ad-hoc sessions scheduling
team.
%
\Person{Michael Banck}
\figgraph{b}{1.0}{images/photos/meet_the_technical_committee}{Meeting
  the Technical Committee}{aigarius}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "dc14-report.en"
%%% End: 
