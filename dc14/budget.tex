%% See README.writers
%% Copyright 2014 Martin Krafft
%% Copyright 2014 Steve Langasek
%% License: CC-BY-SA-3.0
%%
\section{Budgeting}

Budgeting for an event the size of DebConf is a complicated process,
particularly given the broad variation in both the incoming sponsorship and
the number of attendees each year.  But after ten years of running the
conference in the present format, we have a good deal of data to help us
budget effectively.

With an initial seed of USD~36,000 from Debian's general fund, and relatively
low fixed costs for the conference venue, DebConf14 ran a balanced budget at
every step of the process and was able to commit travel sponsorship funds
early, making it possible for sponsored attendees to make cost-effective
travel plans for the benefit of the conference as a whole.

The budgeting, fundraising, bursaries, and accounting teams work together
in the year leading up to the conference, balancing the available resources
to meet the goals of the conference.

\subsection{Budget planning and assumptions}

The budget for DebConf 2014 was based on the assumption that up to 280 people
would attend the conference on any given day with a small amount of
fluctuation, for a total of 308 unique attendees. We furthermore assumed
that roughly 45\% of the participants would request room and board sponsorship,
based on the experiences gathered in previous years.

\begin{center}
  \begin{tabular}{lrr}
    Category & Quantity & Amount [USD]\\
    \hline
    % from initial contract
    Conference venue & fixed & 27,720 \\
    % from budget.ods
    Accommodation (sponsored person-days) & 1,365 & 31,418 \\
    % budget.ods: sum(F8,F10*3,F11*2), sum(F22:F24)
    Food incl. conference dinner (meals) & 3,734 & 38,126 \\
    % from budget.ods incl. speakers
    Travel sponsorship & & 48,000 \\
    Day trip & & 10,884 \\
    Other expenses & & 12,912 \\[0.4em]
    \hline
    Total & & 169,060 \\
  \end{tabular}
  \label{table-budget}
\end{center}

No further costs were budgeted for personnel or organization, as DebConf is
organized by volunteers who graciously donate their time and energy to
the running of the conference.

\pagebreak
\subsection{Sponsorship}

Our fundraising team reached out to a number of first-time sponsors in the
USA, as well as international sponsors, who have steadfastly supported us over
the years. Thanks to the generosity of our sponsors, we broke even well before
the conference began.

Combined with the generous support from professional and corporate attendees,
as well as merchandise sales, we raised a significant surplus, which will be
returned to Debian to be used for funding future Debian Free Software
activities.

\subsection{Final Numbers}

While the overall conference attendance numbers were in line with
expectations, the ratio of sponsored to non-sponsored attendees was
surprisingly low compared to previous editions of the conference.  It is not
clear why so few people availed themselves of accommodation
sponsorship this time, but it did noticeably reduce the conference's expenses
compared to the budget.

Sponsored food participation was also lower than planned. We think that the
variety and quality of Portland's food offerings (see page \pageref{food}) are
the most reasonable explanations for the low turnout. Fortunately, we had
anticipated this possibility and ensured that the cafeteria would only invoice
us for actual consumption.

When it was clear that we would be well within budget, we were able to meet
popular demand by renting an additional room to be used as a ``quiet
hacklab'', and could also arrange for the plenary conference hall to
accomodate a high-profile, last-minute speaker on the second to last night of
the conference.

Together with higher than projected income from professional and corporate
registration fees, the conference ran a substantial surplus, returning to
Debian not only its initial seed of USD~36,000 but also an additional
25~thousand dollars to be used for future events.

The total expenses for the conference amounted to just under USD~147,000
grouped into the following major categories:

\begin{center}
\centering
\begin{tabular}{lrr}
  Category & Budget [USD] & Expense [USD]\\
  \hline
  Conference venue & 27,720 & 32,115 \\
  Accomodation & 31,418 & 27,740 \\
  % 12.400 meals, 2.000 coffee, 9.621 conf dinner, 370 orga drinks
  Food incl. conference dinner & 38,126 & 24,390 \\
  Travel sponsorship & 48,000 & 48,091 \\
  Day trip & 10,884 & 4,850 \\
  % 2.850 t-shirts, 2.000 video team, 3.600 travel, 650 bank, 570 misc
  Other expenses & 12,912 & 9,670 \\[0.4em]
  \hline
  Total & 169,060 & 146,856 \\
\end{tabular}
\label{table-finalaccounting}
\end{center}

The grand total of income for DebConf14 was just under
USD~208,000, divided into the following main categories:

\begin{center}
\begin{tabular}{lr}
  Income Category & Amount [USD]\\
  \hline
  Debian seed & 36,000 \\
  Sponsorship and donations & 155,500 \\
  Professional attendee fees & 16,000 \\
  Sales of merchandise (t-shirts, cups, stickers, etc.) & 390 \\
\end{tabular}
\label{table-incomecategories}
\end{center}

It is important to run a balanced budget for DebConf each year.  It is also
important to run a successful conference each year, shielded from the annual
variability of incoming sponsorship and conference expenses.  DebConf does
not always run a balanced budget without reliance on contributions from the
Debian general coffers.  The surplus from DebConf14 will not only serve as a
seed for future conferences, but also help to provide a necessary buffer
against future budgeting uncertainty.
%
\Person{Steve Langasek, Martin F.\ Krafft}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "dc14-report.en"
%%% End: 
