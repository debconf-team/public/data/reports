We made it again! Despite the technical challenges of bringing enough
cheese and beverages from other hemispheres, the now traditional
"Cheese and Wine Party" was again a tremendous success.

The elaborate preparation even required hiding goat cheese bricks in
pairs of socks, or packing the highly malodorous "Boulette d'Avesnes"
cheese pyramids in seven layers of ultra-hermetic plastic labeled as
"queso pasteurizado" (pasteurized cheese).

Thanks to everyone's ingenuity, we again could gather together the
best cheese and wine from all over the world (the definition of "wine"
being occasionally stretched to accommodate some amazing beverages).

This event has now become a must have social gathering at every
Debconf. So much so, that we should probably institutionalize it with
a Constitutional amendment and create a "CheeseMaster" position to
maintain the high standards of the event.

The organizers gave a rough estimation that we consumed 10-15
kilograms of cheese, 2 dozen bottles of wine and 2-3 dozen bottles of
other amazing beverages.  All this was cut and opened by only 5 knives
and one corkscrew, but eaten, in its entirety, by about 100
participants.

There is currently no estimate of the number of release critical bugs
that were fixed the following night. It is expected to be low and we
estimate that the release of lenny was delayed by one week because of
the event.
