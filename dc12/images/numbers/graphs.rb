#!/usr/bin/ruby
require 'gruff'
graphs = [
          { :title => 'Attendees by year',
            :type => Gruff::Area.new,
            :data => [ ['Attended',    [392, 223, 241, 306, 335, 176] ],
                       ['Reconfirmed', [455, 252, 270, 416, 451, 188] ],
                       ['Registered',  [621, 500, 465, 617, 662, 270] ] ],
            :labels => {0 => '2007', 1 => '2008', 2 => '2009',
              3 => '2010', 4 => '2011', 5 => '2012'} },

          { :title => 'Attendees by region',
            :type => Gruff::Pie.new,
            :data => [ ['Central America', 53 ],
                       ['Rest of the Americas', 40 ],
                       ['Europe', 58 ],
                       ['Asia', 7 ],
                       ['Oceania', 1 ],
                       ['Africa', 1 ],
                       ['Not specified', 15 ] ] },

          { :title => 'Attendees by gender',
            :type => Gruff::StackedBar.new,
            :data => [ ['Male',          [330, 191, 210, 259, 282, 148]],
                       ['Female',        [ 42,  26,  21,  28,  38,  24]],
                       ['Not specified', [ 20,   6,  10,  19,  15,   4]] ],
            :labels => {0 => '2007', 1 => '2008', 2 => '2009',
              3 => '2010', 4 => '2011', 5 => '2012'}
          },

          { :title => 'Attendees by role in Debian',
            :type => Gruff::Pie.new,
            :data => [
                      ['Debian Project Member (all DDs)', 71],
                      ['Debian Maintainer', 8],
                      ['Debian contributor (including artist, translator, etc.)', 22],
                      ['Not yet involved but interested', 34],
                      ['Accompanying a Debian participant', 7],
                      ['None', 7],
                      ['Otherwise involved in Debian', 28]
                     ]
          }
]

# Attendees by gender (third graph) should be made in percentiles;
# total participation numbers are taken from attendees by year (first
# graph, first row)
graphs[2][:data].each { |gender|
  gender[1].each_with_index { |num, year|
    gender[1][year] = num.to_f / graphs[0][:data][0][1][year] * 100
  }
}

graphs.each do |graph|
  tit = graph[:title]
  g = graph[:type]
  g.theme_greyscale
  g.title = tit
  g.replace_colors ["#FDD84E", "#6886B4", "#72AE6E", "#D1695E",
                      "#8A6EAF", "#EFAA43", "#CCCCCC"]
  graph[:data].each do |item|
    next unless item.is_a?(Array)
    g.data(item[0], item[1])
  end
  g.minimum_value = 0;# if g.class == Gruff::Line
  g.labels = graph[:labels]

  filename = tit.downcase.gsub(/ /, '_') + '.png'
  g.write(filename)
end
