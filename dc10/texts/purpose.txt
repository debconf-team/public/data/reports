The role of DebConf

Over the last ten years, DebConf has grown from its origins as a small
meeting on the sidelines of another conference to become a large event
in its own right.  A team of volunteers works all year round, dealing
with administrative aspects of the previous DebConf and preparing for
the next one.  However, it is the conference attendees themselves,
rather than the organizing team, who ensure the success of DebConf,
with benefits for Debian which go beyond the conference period and
beyond the walls of the conference facilities.  DebConf enables
face-to-face interactions between the attendees, broadcasts its talks
live on the web and archives them permanently for future viewing.  It
also provides focused time when attendees can work uninterrupted on
Debian, especially during the DebCamp week before the main conference.

Even brief face-to-face interactions at DebConf can have important
effects by improving subsequent communications between project
contributors.  A short meeting, or even an informal discussion over a
meal, often helps to quickly find solutions to issues which had been
stuck in long circular discussions on mailing lists.  In an
international project where most contributors rarely meet, it can be
hard for contributors to gauge the tone of others online; the
experience of face-to-face interaction with someone makes it much
easier to correctly interpret later written communications.  Each year
we set aside some of the DebConf budget to provide travel bursaries to
some project members who it is important to have at the conference,
and who would not be able to attend out of their own resources - it's
not only the recipients of the bursaries who benefit, but the whole
project.

The talks and discussion sessions at DebConf are important in raising
new proposals for development, in letting contributors know about new
technical advances they can build on in their own work, and in helping
to manage the wide cooperation required for each new Debian release.
While they are important in setting the agenda for those attending
DebConf, they are also watched by many others, during and after the
conference.  Those watching talks live during the conference even have
the opportunity to submit their own questions to speakers, along with
those present in the room.  The recordings are viewed for years
afterwards by Debian contributors and others anywhere in the world.
The live streams and recordings hugely scale up the benefit from money
spent to arrange talks at DebConf.

Anyone visiting the hacklabs during DebConf, at almost any time of day
or night, will find people engrossed in working on Debian - some
silently by themselves typing, others peering at computer screens in
pairs, and others sat in a ring arguing out the technical details of a
design.  Many attendees find it hard to find uninterrupted time to
work on Debian in their regular schedule, and value DebConf as a
period when they can put aside their other usual responsibilities and
focus on Debian work.  The DebCamp period of DebConf is provided
specifically to enable this uninterrupted work - teams may choose to
arrange their own meetings, but there are no centrally scheduled
conference events on these days except for meals, and fewer
people are present than during the main conference, all keen to
progress their work.  The work done on Debian during DebConf has a far
higher value than the amount spent to arrange the conference.

Two other benefits from DebConf are worth pointing out here.  First,
coming to DebConf motivates Debian contributors.  Many attendees find
that their enthusiasm for working on Debian is renewed and increased,
so that the impact of the conference continues long after everyone has
gone home.  Secondly, DebConf motivates the local community in the
region where the conference is held.  Some people attend DebConf
because it happens close to them, without having previously worked on
Debian at all, and then end up becoming Debian Developers.  Some
existing contributors attend DebConf for the first time because it is
close to them, and end up increasing their level of involvement in the
wider Debian project.

DebConf, with all these benefits, is only possible due to the
financial support given by our sponsors.  DebConf sponsorship is
extremely efficient, as the benefits it produces are much greater than
the amounts spent, go far beyond the conference venue, and last long
after the conference period.
