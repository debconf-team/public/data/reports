------------------------------------------------------------------------
						DebConf 11
  						Final Report
------------------------------------------------------------------------

This is the README for the DebConf11 Final Report. It gives a short
description about it, so you know what to do.


This is all based on the DebConf7 report, so if you want to look how it
feels "live", look in the DC7 report. :) 
svn://svn.debian.org/svn/debconf-data/reports/dc7


Tools
-----

The report will be done using LaTeX. If you don't know LaTeX - don't
worry. Just write your stuff as plain text, submit it, someone will
convert it. But feel free to look at existing examples, its easy, you do
not need much LaTeX here. :)

You need to have texlive-latex-base, texlive-latex-recommended,
texlive-latex-extra and texlive-fonts-recommended installed.

Charset
-------

Make sure to use UTF8 when you edit files that dont fit into ASCII. Like
impressions.tex, credits.tex and maybe others too.


Files
-----

There is a dc11-report.tex, this is the masterfile. Except titlepage and
includes it should not contain anything else, everything will simply get
included.

Filenames should be of the format $authorname_$subject.tex, ie. like
samhocevar_wordsfromthedpl.tex, in case he submits a DPL text for the
report.

There is a template.tex file, simply copying that over and start from
there should be enough.

If someone does work for this report or you know did work for DC11 -
please include the name into credits.tex, somewhere at the bottom. If
there is no paragraph for the work they did - add one.

If you add a new file add it to the INCLUDES line in Makefile.


Images
------

Want to add a picture? Fine. Put it into images/, format png or
pdf. Then simply use the following 4 lines at the place
you want it. Feel free to use the options the includegraphics command
and the figure environments/commands offer. You may want to read
http://www.artofproblemsolving.com/LaTeX/AoPS_L_PictHow.php for some
information (or use google :) ).

\begin{figure}[h]
 \includegraphics{images/YOURIMAGE}
 A little description of your image
\end{figure}

Do not include an extension.

A nice place to get pictures from:

http://www.flickr.com/search/?q=debconf11#page=0

Producing output
----------------

Run make. Wait until its finished and you should find dc11-report.pdf,
together with a lot of supporting files, which you can ignore.

make clean gets rid of the supporting files, make veryclean also deletes
the pdf file.

Copyright
---------

Please see copyright.tex
