\section{Budgeting}
\person{Richard Darst, Velimir Iveljić}

Budgeting for DebConf is a long, involved process.  We have incomes in many different currencies and countries, pay many expenses in a local currency, and then reimburse expenses to many attendees all over the world.  Furthermore, we use funds from various Debian accounts, and we need to keep track of the net flow to and from Debian.

This year, we kept all of our records in the journal format of the ledger/hledger programs.  This allowed us to get quick, accurate reports about our current financial situation.  However, these come at a cost of needing to actually keep track of all of this information, which can be hard given the many independent-minded people who are more interested in getting things done than keeping records.

Our budget officer insisted on correct, auditable records for all transactions, and these are reflected in our ledger.  This required cross-checking with account statements from all of our accounts.  Cash transactions also take place, and these are less easy to check on, as those that do them can forget to keep sufficient records, and transaction details can get lost.

% it would be good to have details, but there's no point emphasising it if not
% 
% They kept their 
% detailed budget secret, but the table below shows a rough estimate of 
% their contributions:
%
% Govt. spending table

% Insert data from this URL, and please leave info in LaTeX source:
% 
% http://lists.debconf.org/lurker/message/20110805.100611.960be096.en.html
% Message-ID: <CANmaauheMT3ngSyAmsi4vOy6MsKkdgzi8kXz1NxD1B6xw6100g@mail.gmail.com>

\begin{center}
  \begin{tabular}{|l|l|l|}
    \hline
    \multicolumn{3}{|c|}{Global accounting} \\
    \hline
	Account & Income (EUR) & Expense (EUR) \\ \hline 
	Bank fees & & 258.59                  \\
	Miscellaneous expenses & & 415.71 \\
	Shipping & & 4,550.00 \\
	Taxes & & 839.86 \\
	DebConf Team travel, pre-conference & & 1,058.37 \\
	Video Team & & 698.27 \\
	Video Team shipping & & 365.91 \\
	Travel sponsorship & & 34,464.11 \\
        \textit{DebConf newbies} program & & 2,219.87 \\
	
	Attendee (total) & 19,622.01 & \\
        Corporate registrations & 1,987.24 & \\
        DebCamp registration & 450.00 & \\      
       	Professional registrations & 17,184.77 & \\
        Donations & 562.59 & \\
	Sponsorship & 38,926.12 & \\

	\textbf{Total} & \textbf{59,110.72} & \textbf{44,504.77} \\ \hline
  \end{tabular}
\end{center}

The local organization, DIVA, kept their own records for transactions they handled.  These are listed separately, since they do not have the degree of detail expected of Debian project records.  For areas they handled, DIVA reported only expenses, not details on the income received from local sponsors.

%\begin{table}[htb]
%  \centering
%  \caption{Outstanding liabilities}
%  \begin{tabular}{|l|l|}
%    \hline
%	Outstanding liabilities & Amount in EUR \\
%	Liabilities &  444.18 EUR  \\ \hline
%  \end{tabular}
%\end{table}

%\begin{table}[htb]
%  \centering
%  \caption{To-date returned to Debian}
%  \begin{tabular}{|l|l|}
%    \hline
%	To-date returned to Debian & amount in EUR \\
%	Returned to Debian &  460.01  \\ \hline
%  \end{tabular}
%\end{table}

\begin{center}
  \begin{tabular}{|l|l|}
    \hline
    \multicolumn{2}{|c|}{Local accounting (estimate)}\\
    \hline
	Account & Estimate expense (EUR) \\ \hline 
	Accommodation & 112,095.32                  \\
	Conference dinner & 4,206.33 \\
	Debian Day & 4,541.31 \\
	Insurance & 3,558.23 \\
	T-shirts & 4,055.87 \\
	Pre-DebConf visit & 644.55 \\
	Local transportation & 609.98 \\
	Power solutions through out the Venue & 1,044.59\\
	General expenses & 1,273.63 \\

	\textbf{Total} & \textbf{132,029.81} \\ \hline
  \end{tabular}
\end{table}

% REMARK!
% Since the first time i didn't use the correct converting tool, 
% the ammounts here differ a bit from the ones i've written in the mail above!!!
%
\vskip 1em

Under normal circumstances, we initially budget only for the bare
essentials required for the conference.  Then as we receive more
contributions from sponsors, we gradually allocate money to travel
sponsorship, making the conference more useful by bringing Debian
contributors who could not afford to attend otherwise, and to
improving the ``amenity'' of the conference for attendees, by adding
for example a special conference dinner or a sponsored day-trip if we
have sufficient funds.

\begin{figure}[h]{\textwidth}
  \begin{center}
    \includegraphics[height=6.5cm\textwidth]{images/photos/5970377535_ce5856a090_b}
    \linebreak
    \imcap{Some extra money came from Debian enthusiasts who came with
      Debian merchandise to sell}
  \end{center}
\end{figure}

Travel sponsorship is a special case for our expenses: Given that
DebConf attendees come from every corner of the world, and that many
of them do a lot of work for Debian as volunteers and have jobs from
which they need to take time off (frequently unpaid), every DebConf we
try to help them by sponsoring some of their travel expenses, since
it’s the least we can do for them as a community!

As DebConf is such an important event to the Debian community, and
given the return on the investment the project gets, for the second
time we ran the \textit{DebConf newbies} program: a special travel
sponsorship queue aimed at people who have not previously attended
DebConf. It has proven quite a successful way to foster further
involvement.

This year presented an exception to the usual procedure, thanks to
generous in-kind support from the Government of Republika Srpska, one
of the political entities of Bosnia and Herzegovina.  This in-kind
support included the main conference venue, accommodation and food for
our sponsored attendees, a conference dinner and day outing.  This
allowed us to allocate funds received from other sponsors directly to
areas such as travel sponsorship, without the usual delays.

\begin{figure}[t]{\textwidth}
  \begin{center}
    \includegraphics[height=6.5cm]{images/photos/5970925106_8413e4b958_b}
    \linebreak
    \imcap{DebConf organizers and attendees together with the
      President, Prime Minister and Science and Technology Minister of
      Republika Srpska after the opening of DebConf}
  \end{center}
\end{figure}

Sponsors play a very important part in DebConf organization; indeed, 
without them it would not be possible for us to put on an event of this size. 
All contributions we receive are used to host as many developers 
and other Debian contributors as possible, and to make a good atmosphere for them to work, 
enabling them to further develop and improve Debian as an operating system, 
and also as a community. None of the budget goes on administrative 
costs, since everyone involved in DebConf organization is a volunteer.

Some attendees, in order to support DebConf, pay a Professional or
Corporate registration fee, which is further described in the
Registration section of this report.  This includes a significant
proportion who are sent to DebConf by their employers as part of their
job.  However, our sponsors enable us to allow many other qualifying
Debian contributors to attend DebConf free of charge.  These Debian
contributors include many who are students or from poorer countries,
who could not afford to pay their own hotel bills for the conference,
and would not be able to attend without the program of bursaries made
possible by our sponsors' financial support.  DebConf would be a far
less useful event for Debian if it was only open to those who can
afford to pay their own way, or who are sent as employees.

%\begin{wrapfigure}{r}{0.5\textwidth}
%  \begin{center}
%    \vspace{-20pt}
%    \includegraphics[width=0.48\textwidth]{images/photos/5986860369_fa47b777ef_b}
%    \linebreak
%    \imcap{Help us achieve this ideal situation!}
%    \vspace{-20pt}
%  \end{center}
%\end{wrapfigure}

DebConf receives donations and makes payments in many different 
currencies.  We would like to take this opportunity to thank Software in 
the Public Interest (SPI) and the Verein zur Förderung Freier 
Informationen und Software (FFIS) for being our representative, holding 
money for us in the USA and EU respectively, and for handling payments and 
transactions on our behalf.




% Keep this at the bottom, thanks.
% Local Variables:
% TeX-master: "dc7-report"
% End:
