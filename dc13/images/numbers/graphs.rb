#!/usr/bin/ruby
require 'gruff'
graphs = [
          { :title => 'Attendees by year',
            :type => Gruff::Area.new,
            :sort => true,
            :data => [ ['Attended',    [392, 223, 241, 306, 335, 176, 290] ],
                       ['Reconfirmed', [455, 252, 270, 416, 451, 188, 300] ],
                       ['Registered',  [621, 500, 465, 617, 662, 270, 378] ] ],
            :labels => {0 => '2007', 1 => '2008', 2 => '2009',
              3 => '2010', 4 => '2011', 5 => '2012', 6 => '2013'} },

          { :title => 'Attendees by region',
            :type => Gruff::Pie.new,
            :sort => false,
            :data => [
                       ['Africa', 1 ],
                       ['Americas', 23 ],
                       ['Asia', 13 ],
                       ['Europe', 176 ],
                       ['Oceania', 1 ],
                       ['Not specified', 75 ] ] },

          { :title => 'Attendees by gender',
            :type => Gruff::StackedBar.new,
            :sort => false,
            :data => [
                       ['Female',        [ 42,  26,  21,  28,  38,  24,  26]],
                       ['Not specified', [ 20,   6,  10,  19,  15,   4,  11]],
                       ['Male',          [330, 191, 210, 259, 282, 148, 253]],
                     ],
            :labels => {0 => '2007', 1 => '2008', 2 => '2009',
              3 => '2010', 4 => '2011', 5 => '2012', 6 => '2013'}
          },

          { :title => 'Attendees by role in Debian',
            :type => Gruff::Pie.new,
            :sort => false,
            :data => [
                      ['Debian Project Member (all DDs)', 125],
                      ['Debian contributor (including artist, translator, etc.)', 44],
                      ['Not yet involved but interested', 40],
                      ['Otherwise involved in Debian', 23],
                      ['Debian Maintainer', 21],
                      ['Accompanying a Debian participant', 13],
                      ['None', 24],
                     ]
          }
]

# Attendees by gender (third graph) should be made in percentiles;
# total participation numbers are taken from attendees by year (first
# graph, first row)
graphs[2][:data].each { |gender|
  gender[1].each_with_index { |num, year|
    gender[1][year] = num.to_f / graphs[0][:data][0][1][year] * 100
  }
}

graphs.each do |graph|
  title = graph[:title]
  g = graph[:type]
  g.sort = graph[:sort]
  g.theme_greyscale
  g.title = title
  g.replace_colors ["#FDD84E", "#6886B4", "#72AE6E", "#D1695E",
                      "#8A6EAF", "#EFAA43", "#CCCCCC"]
  graph[:data].each do |item|
    next unless item.is_a?(Array)
    g.data(item[0], item[1])
  end
  g.minimum_value = 0;# if g.class == Gruff::Line
  g.labels = graph[:labels]

  filename = title.downcase.gsub(/ /, '_') + '.png'
  g.write(filename)
end
