% Talks
%% See README.writers

%% Start here:
%% Copyright 2013 Gunnar Wolf, 2014 Bart Massey
%% License: CC-BY-SA-3.0
\section{Talks}
\person{Gunnar Wolf}

What makes a DebConf happen?

Yes, DebConf is about making Debian better. It is also a great social
opportunity. We get to meet face-to-face the people we work with
online every day throughout the year. We get the time to develop
non-technical relationships that often grow into long term, deep
friendships. But before anything else, DebConf is a conference: an
academic gathering structured around a set of peer-submitted,
peer-selected talks.

\figgraph{htbp}{0.8}{images/photos/thierry_raboud-talkroom.jpg}{Main talk room}{thierry_raboud}

Our procedures for talk selection are much more informal than at most
academic gatherings. DebConf is about \textit{sharing with} peers
rather than \textit{rating} them.  Thus, we do not reject talk
proposals, and have a very small set of plenary talks.  That does not
mean that we lack a talks-selection team. A group of volunteers goes
over the talks submitted before the deadline, rates them, and prepares
the preliminary schedule.

Talks are rated according to three main criteria:

\begin{description}
\item[Relevance] {\em How important is this talk to the Debian project?}
  We always get many proposals for talks which are very attractive and
  technically interesting. However, we have to keep the sessions
  relevant to Debian's growth and development.
\item[Novelty] {\em Does this talk present new developments in Debian
  rather than existing technologies?}  While it is important to
  increase awareness of existing technologies, the Debian
  community---as a direct consequence of its geographic dispersion---is
  good both at documenting tools and at educating itself using this
  documentation. DebConf can be seen as the best moment to present new
  analysis, developments and ideas.
\item[Interest] {\em How will a specific talk be received by the Debian
  community?} This final criterion is somewhat subjective; it requires
  the Talks Team members to be intimately familiar with the direction
  and level of discussions within the Debian Project.  The reason for
  this criterion is not to create an artificial \textit{feel-good} set
  of talks. To the contrary, we try to favor topics that will spark
  interesting discussions and that can push important discussions
  forward in this high-bandwidth face-to-face setting.
\end{description}

A defining characteristic of DebConf is that the Talks Team does not
formally approve or reject talks. Instead, we select talks that fill
the tentative schedule, while taking care to leave enough space for
people to propose other talks that were initially submitted, or even
for last-minute ideas.  The only requisite for talk schedule changes is
to inform the Video Team far enough in advance that they can plan and
cover the sessions.  Thus, the DebConf talks schedule is only truly
finalized when the conference is over.  Every year we have
interesting \textit{ad-hoc} talks.

The DebConf talks range over the entire Debian development spectrum.
For 2013, we defined the following tracks:

\begin{description}
\item[Debian For The Cloud] The operating system we produce is among
  the most popular server systems.  In the server space, consolidation
  and cloud computing is the rage.  We had an interesting mix of talks
  about such topics as Debian systems integration with the main
  commercial cloud providers and using Debian as the infrastructure
  upon which to build private clouds.
\item[Building and Porting] Not too long ago, the direction of
  computing seemed to show an inevitable trend towards homogeneity.
  While Debian prides itself in being the most diverse distribution, most of
  the historical hardware architectures for Unix workstations had
  faded away. The future seemed to be dominated by the x86-derived
  instruction set.

  In the last few years, low-power CPUs, particularly ARM and MIPS,
  gained traction from their use in mobile devices, and now seem to
  be making inroads in the desktop and server spaces as well.
  Cross-compilation and multi-architecture systems have once again
  become not only relevant but strategic.

  There are exciting developments in building and porting Debian on
  the software side as well. Linux development has historically been
  identified with the use of the GNU Compiler Collection (GCC).
  However, other free compilers have reached the maturity and
  feature-richness to be considered as serious alternatives, with
  LLVM/Clang leading the pack. Several talks discussed the issues
  here.
\pagebreak
\item[Community Outreach] Debian is more than a technical
  project---it also includes a vital social component.  The
  Community Outreach track included talks on such topics as: how
  Debian can grow socially to be more inclusive, diverse and
  rewarding; how to lower the entrance barrier, making it easier
  to become involved; how Debian can better be integrated into
  the larger Free Software ecosystem; and how Debian and its
  sub-projects (such as DebConf itself!) should be governed.
\item[Real-Time Communications] Free Software seemed to have lost the
  race to bring voice and video-conferencing solutions to end
  users. But several recent developments have again sparked interest
  in this area; there is fresh interest in reliable, secure, and
  user-friendly mechanisms for person-to-person synchronous
  communication.
\item[Debian Blends and Derivatives] Our project seeds literally
  hundreds of derived Linux distributions, as well as many
  \textit{pure blends} (derived distributions developed wholly
  within Debian). This track is one of our constant fixtures,
  presenting some of the most interesting cases.
\item[Quality Assurance] When facing a project as large as Debian, automating
  archive-wide quality checks is always a hot topic. There are many
  Continuous Integration suites that are being integrated into various
  Debian processes, as well as many QA tools specifically developed for
  our workflows and needs.
\item[Debian Boot] The reality of computing changes: what was
  clear and sufficient yesterday will not be enough tomorrow. For the
  last few years, developments both inside the Linux kernel and in the
  general use of computer systems have shown that our
  traditional initialization system, derived straight from
  \textit{Unix System V}, is no longer enough. One of the toughest
  decisions the Debian project faces for our mid-term development is
  choosing which of the alternatives we should adopt.  An entire track
  of talks was sparked by this seemingly too-specific topic.
\item[Debian Teams] Debian is to a large degree structured around
  team-based package maintenance. Most areas of the project's social
  life are also managed in a team-based fashion. DebConf is the
  natural place for these groups to get work done together, but also
  to hold specific meetings, inviting the rest of the project to
  understand their status and challenges. Many of the sessions in
  this track are repeated year after year, and allow us to get the
  pulse of Debian.
\item[Debian's 20th Birthday] Finally, during DebConf13 we celebrated
  Debian's 20th birthday. As part of the celebration, we had a set of
  talks meant for the general public. These talks showed many of the
  ways in which people can get involved, and highlighted interesting
  projects where Debian is one of the players.
\end{description}

Not all of the talks were part of a track. Of the 112 sessions we
scheduled, 41 were presented outside the formal tracks.

On a more personal note, I have personally always tried to be a part
of the Talks Team, and have always considered it a privilege and an
honor. As I was unable to travel to DebConf this year, being involved in
planning the set of talks we would host at least allowed me to play an
important role in the most formally important part of our conference.

Finally, I would like to thank the \textit{Talks Scheduling
  Team}. Just having a list of talks to present is not enough.
Scheduling the talks to minimize the number of sessions an attendee
has to miss due to conflict between talks that they want to hear,
and having a connecting thread between sessions, is difficult to
achieve. Tássia, Tiago and Andreas managed to prepare a great
schedule!
